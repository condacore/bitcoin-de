import { Injectable } from '@angular/core';
import { Platform, AlertController, Events } from 'ionic-angular';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { Storage } from '@ionic/storage';
import "rxjs";
import CryptoJS from 'crypto-js';
import { Http } from '@capacitor-community/http';

@Injectable()
export class RestProvider {

  useragent: string;

  constructor(private platform: Platform, private httpClient: HttpClient, private storage: Storage, private alertCtrl: AlertController, public events: Events) { }

  async doRequest(path: string, http_method: string, parameter_query?: string, post_data: string = ''): Promise<any> {

    try {

      const keys = await this.storage.get('keys');
      const apiKey = JSON.parse(keys)[0];
      const secretKey = JSON.parse(keys)[1];

      const url = 'https://api.bitcoin.de/v4' + path;
      const nonce = String(Date.now() + "0000");
      const post_parameter_md5_hashed_url_encoded_query_string = CryptoJS.MD5(post_data);
      const hmac_data = Array(http_method, url + parameter_query, apiKey, nonce, post_parameter_md5_hashed_url_encoded_query_string).join('#');
      const hmac = String(CryptoJS.HmacSHA256(hmac_data, secretKey));

      const headers = {
        'X-API-KEY': apiKey,
        'X-API-NONCE': nonce,
        'X-API-SIGNATURE': hmac
      };

      // if (this.platform.is('capacitor')) {
        const request = await Http.request({
          method: http_method,
          url: 'https://api.bitcoin.de/v4' + path + parameter_query,
          headers,
          data: post_data
        });
        return request.data;
      // } else {
      //   // GET Function 
      //   if (http_method === 'GET') {
      //     console.log('Using Angular HttpsClient for GET Request');
      //     const resp = await this.httpClient.get('/v4' + path + parameter_query, {
      //       headers
      //     }).toPromise();
      //     return resp;
      //   }
      //   // POST Function 
      //   else if (http_method === 'POST') {
      //     console.log('Using Angular HttpsClient for POST Request');
      //     const resp = await this.httpClient.post('/v4' + path + parameter_query, {
      //       headers
      //     }).toPromise();
      //     return resp;
      //   }
      // }

    } catch (e) {
      this.customErrorAlert('Error', e.error.errors[0].message);
    }
  }

  getChart(pair: string, period: string): Promise<any> {
    let url = 'https://www.bitcoin.de/json/chart/stats_hourly_' + pair + '_statistics_' + period + '.json';
    return this.httpClient.get('https://cors.js-frameworks.com/?' + url).toPromise();
  }

  getBlogFeed(page: any): any {
    let param = '';
    if (typeof page == 'number') {
      param = '%3Fpaged%3D' + page;
    }
    return this.httpClient.get('https://api.rss2json.com/v1/api.json?rss_url=https%3A%2F%2Fbitcoinblog.de%2Ffeed%2F' + param + '%3Fts%3D' + this.guidGenerator());
  }

  getNews(): any {
    return this.httpClient.get('https://bitcoin-news-de.eyloo.workers.dev').toPromise();;
  }

  getLocations(id: any = null): any {
    let parameter;
    if (id) {
      parameter = id;
    } else {
      parameter = '?lat1=47.624674&lat2=54.758460&lon1=-6.300746&lon2=13.926863';
    }
    let url = 'https://coinmap.org/api/v1/venues/' + parameter;
    return this.httpClient.get('https://cors.js-frameworks.com/?' + url + '&type=json');
  }

  getHistory(symbol: any, limit: any = '100'): Promise<any> {
    return this.httpClient.get('https://min-api.cryptocompare.com/data/histoday?fsym=' + symbol + '&tsym=EUR&limit=' + limit).toPromise();
  }

  // We use this whenever we don't want a cached version of a file
  guidGenerator() {
    var S4 = () => {
      return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
  }

  sleep(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  customErrorAlert(title, message) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: ['Schließen']
    });
    alert.present();
  }

}
