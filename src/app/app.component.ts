import { Component, ViewChild, enableProdMode, OnInit } from '@angular/core';
import { Nav, Platform, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Events } from 'ionic-angular';
import { SplashScreen } from '@capacitor/splash-screen';
enableProdMode();

@Component({
  templateUrl: 'app.html'
})
export class MyApp implements OnInit {
  @ViewChild(Nav) nav: Nav;

  rootPage: string = 'HomePage';
  loggedIn: boolean = false;

  pages: Array<{ title: string, component: string, icon: string, login: boolean }>;

  constructor(public platform: Platform, public alertCtrl: AlertController, private storage: Storage, public events: Events) {
    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: 'HomePage', icon: 'home', login: true },
      { title: 'Marktplatz', component: 'MarketplacePage', icon: 'swap', login: true },
      { title: 'Konverter', component: 'ConverterPage', icon: 'md-sync', login: true },
      // { title: 'Karte', component: 'MapPage', icon: 'map', login: false },
      { title: 'News', component: 'NewsPage', icon: 'logo-rss', login: false },
      { title: 'Blog', component: 'BlogPage', icon: 'paper-plane', login: false },
      { title: 'Kontoauszug', component: 'AccountLedgerPage', icon: 'document', login: true },
      { title: 'Meine Trades', component: 'TradesPage', icon: 'pulse', login: true },
    ];

  }

  async ngOnInit() {
    this.events.subscribe('demo:access', (page) => {
      this.nav.setRoot(page);
      this.loggedIn = false;
    });

    this.events.subscribe('first:login', () => {
      this.loggedIn = true;
    });

    const keys = await this.storage.get('keys');
    this.loggedIn = keys ? true : false;
    await SplashScreen.hide();
  }

  openPage(page: any) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    if (this.loggedIn || page.login === false) {
      this.nav.setRoot(page.component);
    } else if (!this.loggedIn && page.login) {
      this.loginNotification();
    }

  }

  logoutConfirm() {
    let alert = this.alertCtrl.create({
      title: 'Ausloggen?',
      message: 'Wenn Sie sich ausloggen werden alle Daten von diesem Gerät gelöscht.',
      buttons: [
        {
          text: 'Abbrechen',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Weiter',
          handler: () => {
            this.logout();
          }
        }
      ]
    });
    alert.present();
  }

  loginNotification() {
    let alert = this.alertCtrl.create({
      title: 'Login erforderlich',
      message: 'Um diese Funktion nutzen zu können ist ein Login mit Ihrem API-Key sowie dem dazugehörigen API-Secret erforderlich. Diese können Sie bei Bitcoin.de erstellen.',
      buttons: [
        {
          text: 'Abbrechen',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Login',
          handler: () => {
            this.logout();
          }
        }
      ]
    });
    alert.present();
  }

  login() {
    this.nav.setRoot('HomePage');
  }


  logout() {
    this.nav.setRoot('HomePage');
    this.storage.clear();
  }

  async sendMail() {
    window.open('mailto:apps@eyloo.com?subject=Bitcoin%20Marktplatz%20App');
  }

}


