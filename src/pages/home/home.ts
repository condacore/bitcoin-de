import { Component, ViewChild, OnInit } from '@angular/core';
import { IonicPage, NavController, Nav, Platform, AlertController, Content, MenuController, LoadingController, Events } from 'ionic-angular';
import { StockChart } from 'angular-highcharts';
import { RestProvider } from '../../providers/rest/rest';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage implements OnInit {

  @ViewChild(Content) content: Content;
  @ViewChild(Nav) nav: Nav;

  stock: StockChart;
  current_rate: any;
  market: string = 'btceur';
  symbol: string = 'btc';
  currency: string = 'Bitcoin';
  balances: any;
  fidor_reservation: any;
  current_value_of_coin: any;

  hideLogin: boolean = true;

  constructor(public navCtrl: NavController, private restProvider: RestProvider, public platform: Platform, private storage: Storage, private alertCtrl: AlertController, public menu: MenuController, public loadingCtrl: LoadingController, public events: Events) {

    // // Check if api key is available. Otherwise show login screen
    // this.storage.get('keysAvailable').then((val) => {
    //   if (!val) {
    //     // Disable menu to prevent app access without keys
    //     this.menu.enable(false);
    //     // Schow the login screen
    //     this.hideLogin = false;
    //     // Resize screen because toolbar to change market was removed
    //     this.content.resize();
    //   } else {
    //     this.hideLogin = true;
    //     this.platform.ready().then(() => {
    //       if (this.hideLogin = true) {
    //         this.initHome();
    //       }
    //     });
    //   }
    // });

  }

  async ngOnInit() {
    // Check if api key is available. Otherwise show login screen
    const keys = await this.storage.get('keys');
    if (keys) {
      this.hideLogin = true;
      this.initHome();
    } else {
      // Disable menu to prevent app access without keys
      this.menu.enable(false);
      // Schow the login screen
      this.hideLogin = false;
      // Resize screen because toolbar to change market was removed
      this.content.resize();
    }
  }

  async initHome() {

    // Show the loading indicator
    const loading = this.loadingCtrl.create({
      duration: 3000
    });
    loading.present();

    // Empty all variables to recive a FadeIn animation from animate.css
    this.stock = null;
    this.current_rate = null;
    this.balances = null;

    // This will aslo trigger laodPrice() & loadBalances()

    await this.loadChart()
    await this.loadPrice()
    await this.loadAccountBalance()

    loading.dismiss();
  }

  private async loadChart() {
    const response = await this.restProvider.getChart(this.market, '48hour');

    let ohlc = new Array();
    for (let item of response) {
      ohlc.push([
        item[0], //time
        item[1],
        item[2],
        item[3],
        item[4]
      ]);
    }
    this.stock = new StockChart(<any>{
      chart: {
        height: (9 / 16 * 100) + '%', // 16:9 ratio
        renderTo: 'container',
      },
      title: {
        text: null
      },
      navigator: {
        enabled: false
      },
      exporting: {
        enabled: false
      },
      credits: {
        enabled: false
      },
      scrollbar: {
        enabled: false
      },
      rangeSelector: {
        //inputEnabled: false,
        enabled: false
      },
      tooltip: {
        split: false,
        followPointer: true,
        followTouchMove: true,
        valueSuffix: ' €',
      },
      series: [{
        type: 'candlestick',
        name: this.market.toUpperCase().replace("EUR", "/EUR"),
        data: ohlc,
        showInLegend: false,
        color: '#ff631b',
        upColor: '#4c7ba6',
      }]
    });
  }

  private async loadPrice() {
    // Load Current Price
    const response = await this.restProvider.doRequest('/' + this.market + '/rates', 'GET', '');

    if (response) {
      this.current_rate = response.rates.rate_weighted;
    }

    switch (this.market) {
      case 'btceur':
        this.currency = 'Bitcoin';
        this.symbol = 'btc';
        break;
      case 'btgeur':
        this.currency = 'Bitcoin Gold';
        this.symbol = 'btg';
        break;
      case 'bcheur':
        this.currency = 'Bitcoin Cash';
        this.symbol = 'bch';
        break;
      case 'etheur':
        this.currency = 'Ethereum';
        this.symbol = 'eth';
        break;
    }

  }

  private async loadAccountBalance() {
    const response = await this.restProvider.doRequest('/account', 'GET', '');

    if (response) {
      this.balances = response.data.balances;
      this.current_value_of_coin = parseFloat(this.current_rate) * parseFloat(this.balances[this.symbol]['total_amount']);
    }
  }

  openHistory() {
    this.navCtrl.push('HistoryPage', {
      currency: this.currency,
      symbol: this.symbol.toUpperCase()
    });
  }

  // Everything below is needed to perform login

  presentErrorAlert() {
    let alert = this.alertCtrl.create({
      title: 'Fehler',
      message: 'Bitte überprüfen Sie Ihre Keys und versichern Sie sich, dass sie ALLE Häkchen bei deren Erstellung gesetzt haben.',
      buttons: ['Schließen']
    });
    alert.present();
  }

  keyValidationError() {
    let alert = this.alertCtrl.create({
      title: 'Ungültige Eingabe',
      message: 'Bitte überprüfen Sie die Vollständigkeit Ihrer Keys.',
      buttons: ['Schließen']
    });
    alert.present();
  }

  presentAlert() {
    let alert = this.alertCtrl.create({
      title: 'API-Keys erstellen',
      subTitle: 'Tippen Sie auf „Weiter“ um auf Bitcoin.de zu gelangen.',
      buttons: [
        {
          text: 'Schließen',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Weiter',
          handler: () => {
            window.open('https://www.bitcoin.de/de/userprofile/tapi/edit');
          }
        }
      ]
    });
    alert.present();
  }

  // Store keys and relead home controller
  async storeKeys(key: string, secret: string) {

    if (key.length === 32 && secret.length === 40) {

      await this.storage.set('keys', JSON.stringify([key, secret]));
      this.events.publish('first:login', key, secret);
      this.hideLogin = true;
      await this.storage.set('keysAvailable', true);
      this.menu.enable(true);
      await this.initHome();
      this.content.resize();

    } else {
      setTimeout(() => {
        this.keyValidationError();
      }, 200);
    }
  }

  demoAccess() {
    this.hideLogin = true;
    this.menu.enable(true);
    this.events.publish('demo:access', 'NewsPage');
  }

}
