import { Component } from '@angular/core';
import { IonicPage, NavController, LoadingController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';

@IonicPage()
@Component({
  selector: 'page-blog',
  templateUrl: 'blog.html',
})
export class BlogPage {

  articles: Array<any>;

  constructor(public navCtrl: NavController, private restProvider: RestProvider, private loadingCtrl: LoadingController) {
    this.loadPosts();
  }

  loadPosts(refresher: any = null, infiniteScroll: any = null) {

    let page;
    let loading;

    if (!refresher && !infiniteScroll) {
      loading = this.loadingCtrl.create({
        duration: 3000
      });
      loading.present();
    }

    if (infiniteScroll) {
      page = this.articles.length / 10 + 1;
    } 

    this.restProvider.getBlogFeed(page).subscribe(data => {
      if (infiniteScroll) {
        // Load more: Push newly loaded coins to current list
        Array.prototype.push.apply(this.articles, data.items);
        infiniteScroll.complete();
      } else if (refresher) {
        refresher.complete();
      } else if (!refresher && !infiniteScroll) {
        // Otherwise replace list with new data
        loading.dismiss();
        this.articles = data.items;
      }
      
    });

  }

  openDetail(article) {
    this.navCtrl.push('BlogDetailPage', {
      article: article
    });
  }

}
