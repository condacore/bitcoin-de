import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BlogPage } from './blog';
import { MomentModule } from 'angular2-moment';
import "moment/locale/de"

@NgModule({
  declarations: [
    BlogPage,
  ],
  imports: [
    MomentModule,
    IonicPageModule.forChild(BlogPage),
  ]
})
export class BlogPageModule {}
