import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController} from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import { Browser } from '@capacitor/browser';

@IonicPage()
@Component({
  selector: 'page-map-detail',
  templateUrl: 'map-detail.html',
})
export class MapDetailPage {

  location: any;
  static_map: string;

  constructor(public navCtrl: NavController, public viewCtrl: ViewController, private restProvider: RestProvider, public navParams: NavParams, public loadingCtrl: LoadingController) {
    this.loadLocation();
  }

  loadLocation(refresher: any = null) {

    let loading = this.loadingCtrl.create({
      duration: 3000
    });
    loading.present();

    this.restProvider.getLocations(this.navParams.get('id')).subscribe(data => {

      this.location = data.venue;
      this.static_map = 'https://maps.googleapis.com/maps/api/staticmap?center=' + data.venue.lat + ',' + data.venue.lon + '&zoom=59&scale=2&size=600x300&maptype=roadmap&format=jpg&visual_refresh=true&markers=size:mid%7Ccolor:0x506688%7Clabel:%7C' + data.venue.lat + ',' + data.venue.lon;

      loading.dismiss();

    });

  }

  async openTwitter(user: string) {
    await Browser.open({ url: 'https://mobile.twitter.com/' + user + '?lang=de' });
  }

  async openFacebook(user: string) {
    await Browser.open({ url: 'https://m.facebook.com/' + user + '?lang=de' });
  }

  async openWebsite(url: string) {
    await Browser.open({ url });
  }

  async openEmail(mail: string) {
    await Browser.open({ url: 'mailto:' + mail });
  }

  async openPhone(number: string) {
    await Browser.open({ url: 'tel:' + number });
  }

  closeModal() {
    this.viewCtrl.dismiss();
  }

}
