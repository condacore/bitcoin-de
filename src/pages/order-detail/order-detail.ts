import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';

@IonicPage()
@Component({
  selector: 'page-order-detail',
  templateUrl: 'order-detail.html',
})
export class OrderDetailPage {

  order: any;
  type: string;
  currency: string;
  symbol: string;
  calculated_value: any;
  order_input: number;

  constructor(public navCtrl: NavController, public navParams: NavParams, private restProvider: RestProvider) {
    this.order = this.navParams.get('order');
    this.type = this.navParams.get('type');

    switch (this.order.trading_pair) {
      case 'btceur':
        this.currency = 'Bitcoin';
        this.symbol = 'BTC';
        break;
      case 'bcheur':
        this.currency = 'Bitcoin Cash';
        this.symbol = 'BCH';
        break;
      case 'btgeur':
        this.currency = 'Bitcoin Gold';
        this.symbol = 'BTG';
        break;
      case 'etheur':
        this.currency = 'Ethereum';
        this.symbol = 'ETH';
        break;
    }

  }

  async buyOrder() {
    const response = await this.restProvider.doRequest('/trades/' + this.order.order_id, 'POST', '', 'amount=' + this.order_input + '&order_id=' + this.order.order_id + '&trading_pair=' + this.order.trading_pair + '&type=buy');
    if (response.errors.length === 0) {
      // Success
    } else {
      // response.errors[0];
    }
  }

  calculatePrice(value = 0.1) {
    //this.calculated_value = value / this.order.price;
    this.calculated_value = value * this.order.price;
    this.order_input = value;

    if (value > this.order.max_amount) {
      this.order_input = this.order.max_amount;
      console.log('Input too high. Resetting it to ' + this.order_input);
    }
  }

  setAmount() {
    this.order_input = this.order.max_amount;
    this.calculated_value = this.order.max_volume;
  }


}
