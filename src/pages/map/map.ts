import { Component, ViewChild } from '@angular/core';
import { IonicPage, Platform, ModalController, LoadingController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import { mapStyle } from '../../providers/mapstyle/mapstyle';

@IonicPage()
@Component({
  selector: 'page-map',
  templateUrl: 'map.html',
})
export class MapPage {

  @ViewChild('map') element;

  constructor(public platform: Platform, private restProvider: RestProvider, private modalCtrl: ModalController, public loadingCtrl: LoadingController) {
  }

  initMap() {

    // let loading = this.loadingCtrl.create({
    //   duration: 3000
    // });
    // loading.present();

    // let mapOptions: GoogleMapOptions = {
    //   controls: {
    //     myLocationButton: true,
    //     compass: false,
    //   },
    //   styles: mapStyle
    // };

    // let map: GoogleMap = GoogleMaps.create(this.element.nativeElement, mapOptions);

    // this.restProvider.getLocations().subscribe(response => {

    //   response.venues.forEach(eachObj => {

    //     map.addMarker({
    //       title: eachObj.name,
    //       snippet: 'Antippen für mehr Infos',
    //       position: {
    //         lat: eachObj.lat,
    //         lng: eachObj.lon
    //       },
    //       icon: {
    //         //url: `assets/map/${eachObj.category}.png`,
    //         url: 'assets/map/markers/' + eachObj.category.replace(" ", "-") + '.png',
    //         size: {
    //           width: 27,
    //           height: 34
    //         }
    //       }
    //     })
    //       .then(marker => {
    //         marker.on(GoogleMapsEvent.MARKER_CLICK)
    //           .subscribe(() => {
    //             this.locationDetail(eachObj.id);
    //           });
    //       });

    //   });
    //   loading.dismiss();
    // });

    // map.one(GoogleMapsEvent.MAP_READY).then((data: any) => {

    //   let coordinates: LatLng = new LatLng(51.165691, 10.451526);

    //   let position = {
    //     target: coordinates,
    //     zoom: 6
    //   };

    //   map.moveCamera(position);
    //   map.setMyLocationEnabled(true);

    // })

  }

  locationDetail(id) {
    let detailModal = this.modalCtrl.create('MapDetailPage', { id: id });
    detailModal.present();
  }

  ionViewDidLoad() {
    this.platform.ready().then(() => {
      this.initMap();
    });
  }

}
