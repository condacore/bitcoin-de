import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, LoadingController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';

@IonicPage()
@Component({
  selector: 'page-trades',
  templateUrl: 'trades.html',
})
export class TradesPage implements OnInit {

  market: string = 'btceur';
  symbol: string = 'BTC';
  response: Array<any>;

  constructor(public navCtrl: NavController, private restProvider: RestProvider, public loadingCtrl: LoadingController) { }

  async ngOnInit() {
    await this.showMyTrades();
  }

  async showMyTrades(refresher: any = null) {

    let loading;

    if (!refresher) {
      loading = this.loadingCtrl.create({
        duration: 3000
      });
      loading.present();
    }

    const response = await this.restProvider.doRequest('/trades', 'GET', '?trading_pair=' + this.market + '&state=1&date_start=2012-01-01T15%3A00%3A00%2B02%3A00');

    this.response = response;
    this.symbol = this.market.replace("eur", "").toUpperCase();

    if (refresher) {
      refresher.complete();
    } else {
      loading.dismiss();
    }

  }

  openDetail(trade: string) {
    this.navCtrl.push('TradesDetailPage', {
      trade: trade,
      symbol: this.symbol
    });
  }

}
