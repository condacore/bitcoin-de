import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TradesPage } from './trades';
import { MomentModule } from 'angular2-moment';
import "moment/locale/de"

@NgModule({
  declarations: [
    TradesPage,
  ],
  imports: [
    MomentModule,
    IonicPageModule.forChild(TradesPage),
  ],
})
export class TradesPageModule {}
