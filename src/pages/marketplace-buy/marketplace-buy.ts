import { Component } from '@angular/core';
import { IonicPage, NavController, ModalController, LoadingController, Events } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest'
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-marketplace-buy',
  templateUrl: 'marketplace-buy.html',
})
export class MarketplaceBuyPage {

  orders = new Array();
  market: any;
  interval: any;
  // counter: any;
  seconds: number = 0;
  animation: boolean = true;
  hideLoading: boolean = false;

  constructor(public navCtrl: NavController, private restProvider: RestProvider, private storage: Storage, public modalCtrl: ModalController, public loadingCtrl: LoadingController, public events: Events) {
    this.events.subscribe('loading:hide', () => {
      this.hideLoading = true;
    });
  }

  async ionViewWillEnter() {
    const market = await this.storage.get('market');

    if (market) {
      this.market = market;
    } else {
      this.market = 'btceur';
      this.storage.set('market', this.market);
    }
    await this.showOrderbook();

    this.animation = true;
    /*this.seconds = 5;
    this.counter = setInterval(() => {
      this.seconds -= 1;
      if(this.seconds < 0) {
        this.seconds = 5
      }
    }, 1000); 
    this.interval = setInterval(() => {
      this.showOrderbook();
    }, 6000);*/

    console.log('Buy refresh interval started');
  }

  async showOrderbook(refresher: any = null) {

    let loading;

    if (!refresher && !this.orders.length) {
      loading = this.loadingCtrl.create({
        duration: 3000
      });
      loading.present();
    }

    const response = await this.restProvider.doRequest('/' + this.market + '/orderbook', 'GET', '?type=buy&only_express_orders=0');

    if (this.orders.length === 0) {
      this.animation = true;
    } else {
      this.animation = false;
    }

    //this.orders = response.orders;
    /*response.orders.forEach(element => {
      this.orders.push(element);
    });*/

    if (response) {
      //this.orders = response.orders.splice(0, response.orders.length, response.orders);
      this.orders = response.orders;
    }

    if (refresher) {
      refresher.complete();
    } else if (loading) {
      loading.dismiss();
    }

    this.storage.set('market', this.market);
  }

  // If a menu item was clicked clear interval
  ngOnDestroy() {
    this.stopInterval();
  }

  // If a Liste item was clicked clear interval
  ionViewWillLeave() {
    this.stopInterval();
  }

  stopInterval() {
    clearInterval(this.interval);
    console.log('Buy refresh interval stopped');
    // clearInterval(this.counter);
  }

  openDetails(order) {
    this.navCtrl.push('OrderDetailPage', {
      order: order,
      type: 'buy'
    });
  }

  presentProfileModal(market) {
    let createOrder = this.modalCtrl.create('ProfilePage',
      /*{
        market: market,
        type: 'buy'
      }*/);
    createOrder.present();
  }

}
