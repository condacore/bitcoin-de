import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MarketplaceBuyPage } from './marketplace-buy';

@NgModule({
  declarations: [
    MarketplaceBuyPage,
  ],
  imports: [
    IonicPageModule.forChild(MarketplaceBuyPage),
  ]
})
export class MarketplaceBuyPageModule {}
