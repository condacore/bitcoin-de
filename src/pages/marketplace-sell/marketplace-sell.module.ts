import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MarketplaceSellPage } from './marketplace-sell';

@NgModule({
  declarations: [
    MarketplaceSellPage,
  ],
  imports: [
    IonicPageModule.forChild(MarketplaceSellPage),
  ]
})
export class MarketplaceSellPageModule {}
