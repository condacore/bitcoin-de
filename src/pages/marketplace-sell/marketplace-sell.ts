import { Component } from '@angular/core';
import { IonicPage, NavController, ModalController, LoadingController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest'
import { Storage } from '@ionic/storage';


@IonicPage()
@Component({
  selector: 'page-marketplace-sell',
  templateUrl: 'marketplace-sell.html',
})
export class MarketplaceSellPage {

  orders = new Array();
  market: any;
  interval: any;
  // counter: any;
  seconds: number = 0;
  animation: boolean = true;

  constructor(public navCtrl: NavController, private restProvider: RestProvider, private storage: Storage, public modalCtrl: ModalController, private loadingCtrl: LoadingController) {
  }

  async ionViewWillEnter() {
    const market = await this.storage.get('market');
    if (market) {
      this.market = market;
    } else {
      this.market = 'btceur';
      this.storage.set('market', this.market);
    }
    this.showOrderbook();
    /* this.animation = true;
    this.seconds = 5;
    this.counter = setInterval(() => {
      this.seconds -= 1;
      if(this.seconds < 0) {
        this.seconds = 5
      }
    }, 1000); 
    this.interval = setInterval(() => {
      this.showOrderbook();
    }, 6000);

    console.log('Sell refresh interval started');*/
  }

  async showOrderbook(refresher: any = null) {

    let loading;

    if (!refresher && !this.orders.length) {
      loading = this.loadingCtrl.create({
        duration: 3000
      });
      loading.present();
    }

    const response = await this.restProvider.doRequest('/' + this.market + '/orderbook', 'GET', '?type=sell&only_express_orders=0');

    if (this.orders.length === 0) {
      this.animation = true;
    } else {
      this.animation = false;
    }

    if (response) {
      this.orders = response.orders;
    }

    if (refresher) {
      refresher.complete();
    } else if (loading) {
      loading.dismiss();
    }

    this.storage.set('market', this.market);
  }

  // If a menu item was clicked clear interval
  ngOnDestroy() {
    this.stopInterval();
  }

  // If a Liste item was clicked clear interval
  ionViewWillLeave() {
    this.stopInterval();
  }

  stopInterval() {
    clearInterval(this.interval);
    console.log('Sell refresh interval stopped');
    // clearInterval(this.counter);
  }

  openDetails(order) {
    this.navCtrl.push('OrderDetailPage', {
      order: order,
      type: 'sell'
    });
  }


  presentProfileModal(market) {
    let createOrder = this.modalCtrl.create('ProfilePage',
      /*{
        market: market,
        type: 'sell'
      }*/);
    createOrder.present();
  }


}
