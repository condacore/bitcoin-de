import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, LoadingController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import { Browser } from '@capacitor/browser';
import { OrderPipe } from 'ngx-order-pipe';

@IonicPage()
@Component({
  selector: 'page-news',
  templateUrl: 'news.html',
})
export class NewsPage implements OnInit {

  articles: Array<any>;

  constructor(public navCtrl: NavController, private restProvider: RestProvider, private loadingCtrl: LoadingController, private orderPipe: OrderPipe) { }

  async ngOnInit() {
    await this.loadArticles();
  }

  async loadArticles(refresher: any = null) {

    let loading;

    if (!refresher) {
      loading = this.loadingCtrl.create({
        duration: 3000
      });
      loading.present();
    }

    const articles = await this.restProvider.getNews();

    this.articles = this.orderPipe.transform(articles, 'publishedAt', true);

    if (refresher) {
      refresher.complete();
    } else if (loading) {
      loading.dismiss();
    }

  }

  async openDetail(url) {
    await Browser.open({ url });
  }

}
