import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewsPage } from './news';
import { OrderModule } from 'ngx-order-pipe';

@NgModule({
  declarations: [
    NewsPage,
  ],
  imports: [
    OrderModule,
    IonicPageModule.forChild(NewsPage),
  ]
})
export class NewsPageModule {}
