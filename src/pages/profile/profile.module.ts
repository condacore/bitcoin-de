import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProfilePage } from './profile';

import { MomentModule } from 'angular2-moment';
import "moment/locale/de"

@NgModule({
  declarations: [
    ProfilePage,
  ],
  imports: [
    MomentModule,
    IonicPageModule.forChild(ProfilePage),
  ],
})
export class ProfilePageModule {}
