import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, ViewController, LoadingController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage implements OnInit {

  balances: any;
  fidor_reservation: any;
  total: number;
  symbol: string = 'btc';
  current_value: any = 0;

  constructor(public navCtrl: NavController, private restProvider: RestProvider, public viewCtrl: ViewController, public loadingCtrl: LoadingController) { }

  async ngOnInit() {
    await this.loadProfile();
  }

  async loadProfile() {

    let loading = this.loadingCtrl.create({
      duration: 3000
    });
    loading.present();

    const response = await this.restProvider.doRequest('/account', 'GET', '');
    this.fidor_reservation = response?.data?.fidor_reservation;

    loading.dismiss();
  }

  async calcValue(path: string = '/rates', http_method: string = 'GET', parameter_query: string = '?trading_pair=' + this.symbol + 'eur') {
    const response = await this.restProvider.doRequest(path, http_method, parameter_query);
    this.current_value = parseFloat(response.rates.rate_weighted) * parseFloat(this.balances[this.symbol]['total_amount']);
  }

  openTradesPage(page) {
    this.navCtrl.push(page);
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
