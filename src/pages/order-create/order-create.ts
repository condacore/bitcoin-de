import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest'
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-order-create',
  templateUrl: 'order-create.html',
})

export class OrderCreatePage {

  type: string;
  currency: string;
  symbol: string;
  current_price: string;
  trading_pair: string;
  current_rate: string;

  min_trust_level: string = 'bronze';
  volume: any = 0;
  amount: any;

  order: FormGroup;

  end_time: any;
  end_day: string;
  time_now: string;

  country_bank: string = 'DE';
  country_settings: string = 'all';

  countries = [
    ['Deutschland', 'DE'],
    ['Österreich', 'AT'],
    ['Belgien', 'BE'],
    ['Bulgarien', 'BG'],
    ['Schweiz', 'CH'],
    ['Zypern', 'CY'],
    ['Tschechische Republik', 'CZ'],
    ['Dänemark', 'DK'],
    ['Estland', 'EE'],
    ['Spanien', 'ES'],
    ['Finnland', 'FI'],
    ['Frankreich', 'FR'],
    ['Großbritannien', 'GB'],
    ['Griechenland', 'GR'],
    ['Kroatien', 'HR'],
    ['Ungarn', 'HU'],
    ['Irland', 'IE'],
    ['Island', 'IS'],
    ['Italien', 'IT'],
    ['Liechtenstein', 'LI'],
    ['Litauen', 'LT'],
    ['Luxemburg', 'LU'],
    ['Lettland', 'LV'],
    ['Malta', 'MT'],
    ['Martinique', 'MQ'],
    ['Niederlande', 'NL'],
    ['Norwegen', 'NO'],
    ['Polen', 'PL'],
    ['Portugal', 'PT'],
    ['Rumänien', 'RO'],
    ['Schweden', 'SE'],
    ['Slowenien', 'SI'],
    ['Slowakei', 'SK'],
  ];


  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, private restProvider: RestProvider, private formBuilder: FormBuilder, public loadingCtrl: LoadingController) {

    this.trading_pair = this.navParams.get('market');
    this.type = this.navParams.get('type');

    switch (this.trading_pair) {
      case 'btceur':
        this.currency = 'Bitcoin';
        this.symbol = 'BTC';
        break;
      case 'bcheur':
        this.currency = 'Bitcoin Cash';
        this.symbol = 'BCH';
        break;
      case 'btgeur':
        this.currency = 'Bitcoin Gold';
        this.symbol = 'BTG';
        break;
      case 'etheur':
        this.currency = 'Ethereum';
        this.symbol = 'ETH';
        break;
    }


    this.time_now = this.calculateTime('+1').split('T')[0] + 'T' + new Date().getHours() + ':00:00';
    console.log('TIME NOW: ' + this.time_now)
    this.end_time = new Date().getHours() + 2 + ':00:00';
    this.end_day = this.calculateTime('+23');

    // If it is Daylight Savings Time
    if (this.dst(new Date())) {
      this.time_now = this.calculateTime('+2');
      this.end_day = this.calculateTime('+24');
    }

    this.order = this.formBuilder.group({
      max_amount: [null, Validators.required], // required
      min_amount: [null, Validators.required],
      price: [null], // required
      day: [this.calculateTime('+23')],
      time: [new Date().getHours() + 2 + ':00:00'],
      min_trust_level: ['bronze'],
      seat_of_bank: [null],
      payment_option: [null],
      new_order_for_remaining_amount: [true],
      only_kyc_full: [true],
    });

    this.getPrice();
  }

  validateForm() {
    let loading = this.loadingCtrl.create({
      content: 'Prüfen...'
    });
    loading.present();

    let query;

    query = 'end_datetime=' + this.order.value.day.split('T')[0] + 'T' + this.order.value.time;
    query += '&max_amount=' + this.order.value.max_amount;
    query += '&min_amount=' + this.order.value.min_amount;
    query += '&min_trust_level=' + this.order.value.min_trust_level;

    if (this.order.value.new_order_for_remaining_amount) {
      query += '&new_order_for_remaining_amount=1';
    }
    if (this.order.value.only_kyc_full) {
      query += '&only_kyc_full=1';
    }
    query += '&price=' + this.order.value.price;

    if (this.order.value.seat_of_bank && this.country_settings == 'custom') {
      query += '&seat_of_bank=' + this.order.value.seat_of_bank;
    }
    query += '&trading_pair=' + this.trading_pair;
    query += '&type=' + this.type;

    this.createOrder(query);

    loading.dismiss();
    console.log('Query: ' + query);
  }

  logForm() {
    console.log(this.order.value.max_amount)
  }

  notify() {
    //
  }

  async getPrice() {
    const response = await this.restProvider.doRequest('/rates', 'GET', '?trading_pair=' + this.trading_pair);
    this.current_rate = response.rates.rate_weighted;
  }

  async createOrder(query: string) {
    const response = await this.restProvider.doRequest('/orders', 'POST', '', query);
    this.current_rate = response.rates.rate_weighted;
  }

  /*onInputPrice(value) {
    this.amount * value;
  }*/

  onInputAmount(value) {
    this.volume = parseFloat(this.current_rate) * value;
    //this.volume = this.currencyPipe.transform(volume2, 'EUR','symbol','1.2-2','de');
  }


  dismiss() {
    this.viewCtrl.dismiss();
  }

  addDays(days) {
    var result = new Date();
    result.setDate(result.getDate() + days);
    return result;
  }

  // Time Management

  calculateTime(offset: any) {
    // create Date object for current location
    let d = new Date();

    // create new Date object for different city
    // using supplied offset
    let nd = new Date(d.getTime() + (3600000 * offset));

    return nd.toISOString();
  }

  // Determine if the client uses DST
  stdTimezoneOffset(today: any) {
    let jan = new Date(today.getFullYear(), 0, 1);
    let jul = new Date(today.getFullYear(), 6, 1);
    return Math.max(jan.getTimezoneOffset(), jul.getTimezoneOffset());
  }

  dst(today: any) {
    return today.getTimezoneOffset() < this.stdTimezoneOffset(today);
  }
}
