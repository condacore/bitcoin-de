import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrderCreatePage } from './order-create';
import { CurrencyPipe } from '@angular/common';

@NgModule({
  declarations: [
    OrderCreatePage,
  ],
  imports: [
    IonicPageModule.forChild(OrderCreatePage),
  ],
  providers: [
    CurrencyPipe
  ]
})
export class OrderCreatePageModule {}
