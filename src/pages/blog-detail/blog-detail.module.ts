import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BlogDetailPage } from './blog-detail';
import { MomentModule } from 'angular2-moment';
import "moment/locale/de"

@NgModule({
  declarations: [
    BlogDetailPage,
  ],
  imports: [
    MomentModule,
    IonicPageModule.forChild(BlogDetailPage),
  ]
})
export class BlogDetailPageModule {}
