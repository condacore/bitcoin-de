import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Share } from '@capacitor/share';

@IonicPage()
@Component({
  selector: 'page-blog-detail',
  templateUrl: 'blog-detail.html',
})
export class BlogDetailPage {

  article: Array<any>;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.article = this.navParams.get('article');
  }

  async shareArticle(article) {
    try {
      await Share.share({
        title: article.title,
        text: article.description,
        url: article.link,
        dialogTitle: 'Beitrag teilen',
      });
    } catch { }
  }

}
