import { Component, OnInit } from '@angular/core';
import { IonicPage, LoadingController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';

@IonicPage()
@Component({
  selector: 'page-converter',
  templateUrl: 'converter.html',
})
export class ConverterPage implements OnInit {

  market: string = 'btceur';
  symbol: string = 'BTC';
  price: any;
  result: number = 0;
  input: number;
  converter: string = 'a-b';

  constructor(private restProvider: RestProvider, public loadingCtrl: LoadingController) { }

  async ngOnInit() {
    await this.loadPrice();
  }

  async loadPrice() {

    let loading = this.loadingCtrl.create({
      duration: 3000
    });
    loading.present();

    const response = await this.restProvider.doRequest('/' + this.market + '/rates', 'GET', '');

    if (response) {
      this.price = parseFloat(response.rates.rate_weighted).toFixed(2);
    }

    this.symbol = this.market.replace("eur", "").toUpperCase();

    if (this.converter == 'a-b' && this.input) {
      this.calculateAB();
    } else if (this.converter == 'b-a' && this.input) {
      this.calculateBA();
    }

    loading.dismiss();
  }

  swap() {
    if (this.converter == 'a-b') {
      this.converter = 'b-a';
    } else if (this.converter == 'b-a') {
      this.converter = 'a-b';
    }

    [this.input, this.result] = [this.result, this.input];
  }

  // EUR to BTC
  calculateAB() {
    let result = this.input / parseFloat(this.price);
    this.result = parseFloat(result.toFixed(8));
  }

  // BTC to EUR
  calculateBA() {
    this.result = this.input * parseFloat(this.price);
  }




}
