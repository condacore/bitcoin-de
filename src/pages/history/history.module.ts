import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HistoryPage } from './history';
import { MomentModule } from 'angular2-moment';
import "moment/locale/de"

@NgModule({
  declarations: [
    HistoryPage,
  ],
  imports: [
    MomentModule,
    IonicPageModule.forChild(HistoryPage),
  ]
})
export class HistoryPageModule {}
