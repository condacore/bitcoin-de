import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';

@IonicPage()
@Component({
  selector: 'page-history',
  templateUrl: 'history.html',
})
export class HistoryPage {

  history: Array<any>;
  currency: string;
  symbol: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, private restProvider: RestProvider, private loadingCtrl: LoadingController) {
    this.symbol = this.navParams.get('symbol');
    this.currency = this.navParams.get('currency');

    this.loadHistory();

  }

  async loadHistory(infiniteScroll: any = null) {

    const loading = this.loadingCtrl.create({
      duration: 3000
    });
    loading.present();

    const response = await this.restProvider.getHistory(this.symbol);
    setTimeout(() => {
      this.history = response.Data.reverse();
      loading.dismiss();
    }, 500);
  }

  async doInfinite(infiniteScroll: any = null) {
    const limit = this.history.length + 100;
    const response = await this.restProvider.getHistory(this.symbol, limit);
    this.history = response.Data.reverse();
    infiniteScroll.complete();
  }

}
