import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-marketplace',
  templateUrl: 'marketplace.html'
})
export class MarketplacePage {

  buyRoot = 'MarketplaceBuyPage'
  sellRoot = 'MarketplaceSellPage'

  chatParams = {
    user1: 'admin',
    user2: 'ionic'
  };

  constructor(public navCtrl: NavController) {
  }

}
