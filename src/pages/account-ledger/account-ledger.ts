import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, LoadingController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import { Browser } from '@capacitor/browser';

@IonicPage()
@Component({
  selector: 'page-account-ledger',
  templateUrl: 'account-ledger.html',
})
export class AccountLedgerPage implements OnInit {

  response: any;
  symbol: string = 'BTC';
  market: string = 'btceur';

  constructor(public navCtrl: NavController, private restProvider: RestProvider, public loadingCtrl: LoadingController) { }

  async ngOnInit() {
    await this.showAccountLedger();
  }

  async showAccountLedger(refresher: any = null) {

    let loading;

    if (!refresher) {
      loading = this.loadingCtrl.create({
        duration: 3000
      });
      loading.present();
    }

    const response = await this.restProvider.doRequest('/' + this.market.replace("eur", "") + '/account/ledger', 'GET', '?datetime_start=2012-01-01T15%3A00%3A00%2B02%3A00');

    this.response = response;
    this.symbol = this.market.replace("eur", "").toUpperCase();

    if (refresher) {
      refresher.complete();
    } else {
      loading.dismiss();
    }
  }

  async openDetail(reference?: string) {
    if (!reference) {
      // Nothing
    } else if (reference.length === 6) {
      this.navCtrl.push('TradesDetailPage', {
        reference: reference,
        tradingPair: this.market
      });
    } else {
      switch (this.market) {
        case 'btceur':
          await Browser.open({ url: 'https://blockchain.info/de/tx/' + reference });
          break;
        case 'bcheur':
          await Browser.open({ url: 'https://blockchair.com/bitcoin-cash/transaction/' + reference });
          break;
        case 'btgeur':
          await Browser.open({ url: 'https://btgexp.com/tx/' + reference });
          break;
        case 'etheur':
          await Browser.open({ url: 'https://etherscan.io/tx/' + reference });
          break;
      }
    }
  }

}
