import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AccountLedgerPage } from './account-ledger';
import { MomentModule } from 'angular2-moment';
import "moment/locale/de"

@NgModule({
  declarations: [
    AccountLedgerPage,
  ],
  imports: [
    MomentModule,
    IonicPageModule.forChild(AccountLedgerPage),
  ]
})
export class AccountLedgerPageModule {}
