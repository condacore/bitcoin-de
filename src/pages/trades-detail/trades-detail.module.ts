import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TradesDetailPage } from './trades-detail';

import { MomentModule } from 'angular2-moment';
import "moment/locale/de"

@NgModule({
  declarations: [
    TradesDetailPage,
  ],
  imports: [
    MomentModule,
    IonicPageModule.forChild(TradesDetailPage),
  ],
})
export class TradesDetailPageModule {}
