import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';

@IonicPage()
@Component({
  selector: 'page-trades-detail',
  templateUrl: 'trades-detail.html',
})
export class TradesDetailPage implements OnInit {

  trade: any;
  symbol: string;
  tradingPair: string;
  reference: string; // (same as trade_id)

  constructor(public navCtrl: NavController, public navParams: NavParams, private restProvider: RestProvider, public loadingCtrl: LoadingController) {
    this.trade = this.navParams.get('trade');
    this.symbol = this.navParams.get('symbol');
    // The following params are given if user comes from account ledger page
    this.tradingPair = this.navParams.get('tradingPair');
    this.reference = this.navParams.get('reference');
  }

  async ngOnInit() {
    if (!this.trade) {
      await this.getTrade();
    }
  }

  async getTrade() {

    let loading = this.loadingCtrl.create({
      duration: 3000
    });
    loading.present();

    const response = await this.restProvider.doRequest('/' + this.tradingPair + '/trades/' + this.reference, 'GET', '?trading_pair=' + this.tradingPair + '&state=1&date_start=2012-01-01T15%3A00%3A00%2B02%3A00');

    this.trade = response.trade;
    loading.dismiss();

  }
}
